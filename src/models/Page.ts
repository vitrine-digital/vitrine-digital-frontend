export default class Page<T> {
  items: T[];
  itemCount: number;
  totalItems: number;
  pageCount: number;
  next: string;
  previous: string;

  static build(): Page<any> {
    return {
        items: [],
        itemCount: 0,
        totalItems: 0,
        pageCount: 0,
        next: '',
        previous: ''
    };
  }
}

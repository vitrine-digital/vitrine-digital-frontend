import { Route } from 'vue-router';
import { AuthStore } from '../store/auth.store';
import { AuthService } from '../services/auth.service';
import { decodeToken, openSnack } from './utils';

export function authGuard(to: Route, from: Route, next: any) {
	const service = new AuthService();
	if (AuthStore.usuario === null) {
		if (localStorage.getItem('accessToken')) {
			let token = JSON.stringify(localStorage.getItem('accessToken'));
			if (decodeToken(token).exp < Math.round(new Date().getTime() / 1000)) {
				if (to.path == '/login') return next();
				return next('/login');
			}
			AuthStore.setToken(JSON.parse(token));
			return service
				.checkToken()
				.then((payload) => {
					caches.delete('check');
					AuthStore.userChecked(payload.data);
					if (to.path == '/login') return next('/produtos');
					return next();
				})
				.catch(() => {
					return next('/login');
				});
		} else {
			// Não tem token
			if (to.path == '/login') return next();
			return next('/login');
		}
	} else {
		if (to.path == '/login') {
			return next(from.path);
		}
		return next();
	}
}

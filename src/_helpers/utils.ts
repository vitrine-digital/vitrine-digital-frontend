import EventBus from './eventBus';

export function decodeToken(token: string) {
  try {
    return JSON.parse(atob(token.split(".")[1]));
  } catch (e) {
    return null;
  }
}

export function openSnack(data: {msg: string, color?: string}){
  EventBus.$emit('snack', data);
}

export function valueComparator(a: Base, b: Base){
  if(a && b) {
    return a.id == b.id;
  }
  return false;
}

function buildFormData(formData, data, parentKey?: any) {
  if (
    data &&
    typeof data === 'object' &&
    !(data instanceof Date) &&
    !(data instanceof File)
  ) {
    Object.keys(data).forEach((key) => {
      buildFormData(
        formData,
        data[key],
        parentKey ? `${parentKey}[${key}]` : key
      );
    });
  } else {
    const value = data == null ? '' : data;
    formData.append(parentKey, value);
  }
}

export function jsonToFormData(data) {
  const formData = new FormData();
  buildFormData(formData, data);
  return formData;
}

interface Base {
  id: number;
}

import { Usuario } from '@/models/usuario';
import { Validator } from '@/_helpers/validators';

export function validadoresPadraoUsuario(usuario?: Usuario) {
	if (!usuario) {
        usuario = new Usuario();
	}
	return {
		nome: [Validator.required()],
		sobrenome: [Validator.required()],
		email: [Validator.required(), Validator.email()],
		senha: usuario.id ? [] : [Validator.required()],
		cpf: [Validator.required()],
		grupoDePesquisa: [Validator.required()],
		citacao: [], // TODO: Adicionar novas propriedades no modelo
		matricula: [],
		lattes: [],
		orientador: [],
		coorientador: [],
	};
}

export function opcoesPesquisaProfessor() {
	const opcoes = new Map<string, string>();

	opcoes.set("docente", "true")

	return Object.fromEntries(opcoes);
}

export function definirLabelDocente(usuario: Usuario): string  {
	return `${usuario.nome} ${usuario.sobrenome} - ${usuario.grupoDePesquisa.nome}`
}

import { Route } from 'vue-router';
import { ProdutoStore } from '@/store/produto.store';
import { Produto } from '@/models/produto';
import GenericService from '@/services/generic.service';

const service = new GenericService<Produto>('produtos');

export async function produtoResolver(to: Route, from: Route, next: any) {
	const id = to.params.id as any;
	if (id) {
		let produto: Produto = ProdutoStore.getOne(id);
		if (!produto) {
			produto = (await service.findById(id)).data;
			produto.files = [];
			ProdutoStore.addOne(produto);
		}
		to.meta['produto'] = produto;
		next();
	}
}

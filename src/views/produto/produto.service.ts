import GenericService from '@/services/generic.service';
import { Produto } from '@/models/produto';
import api from '@/_helpers/axios';

export class ProdutoService extends GenericService<Produto> {
    constructor() {
        super('produtos');
    }

    saveImagens(id: number, form: any) {
        const header = {
            headers: {
                'Content-type': 'multipart/form-data',
            },
        }
        return api.post<Produto>(`${this.endpoint}/${id}/imgs`, form, header);
    }

    
}
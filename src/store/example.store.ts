import { GetterTree, ActionTree, MutationTree } from 'vuex';
import {AuthService} from '@/services/auth.service';
import { Usuario } from '@/models/usuario';
import { UsuarioLogin } from '@/models/usuarioLogin';

interface UsuarioState {
    usuario: Usuario | null
    loging: boolean | false
    token: number | null
}

const service = new AuthService();

// const usuarioAdapter = new EntityAdapter<Usuario>();
// TODO: Este é um exemplo de store criado da maneira tradicional

const state: UsuarioState = {
    usuario: null,
    loging: false,
    token: null
};

const getters: GetterTree<UsuarioState, any> = {
    token: state => state.token,
    usuario: state => state.usuario,
    logging: state => state.loging
}

const mutations: MutationTree<UsuarioState> = {
    logged(state, payload: UsuarioState) {
        state.usuario = payload.usuario;
        state.token = payload.token;
    }
}

const actions: ActionTree<UsuarioState, any> = {
    initLogin({commit}, dadosLogin: UsuarioLogin) {
        state.loging = true;
        service.login(dadosLogin)
            .then(
                res => {
                    commit('listar', res.data);
                    state.loging = false;
                }
            )
            .catch(
                err => {
                    state.loging = false;
                }
            )
    }
}

export const authStore = {namespaced: true, state, mutations, actions, getters}
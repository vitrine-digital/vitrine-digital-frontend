import { GrupoDePesquisa } from './grupoDePesquisa';
import { Usuario } from './usuario';
import { FichaTecnica } from './fichaTecnica';
import { Imagem } from './imagem';
import { Video } from './video';

export class Produto {
    id: number;
    titulo: string;
    ativo: boolean;
    grupoDePesquisa: GrupoDePesquisa
    palavrasChave: any[]
    site: string;
    proprietario?: Usuario;
    fichaTecnica: FichaTecnica;
    imagens: Imagem[];
    videos: Video[]
    files: any[] = [];
    paragrafos: any[];
    orientador: Usuario;
    coorientador: Usuario;
}
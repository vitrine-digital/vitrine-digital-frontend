import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules';
import { EntityState, EntityAdapter } from 'vue-entity-adapter-plus';
import store from './index';
import GenericService from '@/services/generic.service';
import Page from '@/models/Page';
import { openSnack } from '@/_helpers/utils';
import { Usuario } from '@/models/usuario';
import QueryOptions from '@/models/QueryOptions';

interface State extends EntityState<Usuario> {
	alterado: Usuario;
	loadingList: boolean;
	totalItens: number;
	auxiliar: EntityState<Usuario>
}

const stateAdapter = new EntityAdapter<Usuario>();
const service = new GenericService<Usuario>('usuarios');

@Module
class UsuarioVuex extends VuexModule {
	// state
	state: State = {
		...stateAdapter.getInitialState(),
		alterado: null,
		loadingList: false,
		totalItens: 0,
		auxiliar: {
			...stateAdapter.getInitialState()
		}
	};

	// getters
	get usuarios() {
		return stateAdapter.getAll(this.state);
	}

	get auxiliar() {
		return stateAdapter.getAll(this.state.auxiliar);
	}

	get carregando() {
		return this.state.loadingList;
	}

	get alterado() {
		return this.state.alterado;
	}

	get totalItens(){
		return this.state.totalItens;
	}

	getOne(id: string | number) {
		return stateAdapter.getOne(id.toString(), this.state);
	}

	// mutations
	@Mutation
	private listado(payload: Page<Usuario>) {
		let { entities, ids } = stateAdapter.addMany(payload.items, this.state);
		this.state.entities = entities;
		this.state.ids = ids;
		this.state.totalItens = payload.totalItems;
	}

	@Mutation
	private listadoGenerico(payload: Page<Usuario>) {
		let { entities, ids } = stateAdapter.addMany(payload.items, this.state.auxiliar);
		this.state.auxiliar.entities = entities;
		this.state.auxiliar.ids = ids;
	}

	@Mutation
	private salvo(payload: Usuario) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Item cadastrado com sucesso!', color: 'success' });
	}

	@Mutation
	private atualizado(payload: Usuario) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Item atualizado com sucesso!', color: 'success' });
	}

	@Mutation
	private setadoAlterado(payload: Usuario) {
		this.state.alterado = payload;
	}

	@Mutation
	private findedByID(payload: Usuario) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
	}

	@Mutation
	private imagensSalvas(payload: Usuario) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Imagem(s) salva(s) com sucesso!', color: 'success' });
	}

	// actions
	@Action
	async listar(params: any) {
		this.state.loadingList = true;
		service
			.list(params)
			.then((res) => {
				this.listado(res.data);
				this.state.loadingList = false;
			})
			.catch((err) => {
				this.state.loadingList = false;
			});
	}

	@Action
	async listarGenerico(options: QueryOptions) {
		const {pagination, query} = options
		service
			.list({...pagination, ...query})
			.then((res) => {
				this.listadoGenerico(res.data);
			})
			.catch((err) => {
				this.state.loadingList = false;
			});
	}

	@Action
	async salvar(usuario: Usuario) {
		service
			.save(usuario)
			.then((res) => {
				this.salvo(res.data);
			})
			.catch((err) =>
				openSnack({ msg: 'Erro ao cadastrar item', color: 'error' })
			);
	}

	@Action
	async atualizar(usuario: Usuario) {
		service
			.update(usuario)
			.then((res) => {
				this.atualizado(res.data);
			})
			.catch((err) =>
				openSnack({ msg: 'Erro ao atualizar item', color: 'warn' })
			);
	}

	@Action
	setAlterado(usuario: Usuario) {
		this.setadoAlterado(usuario);
	}

	@Action
	findById(id: number) {
		service
			.findById(id)
			.then((res) => {
				this.findedByID(res.data);
			})
			.catch((err) => openSnack({ msg: 'Erro ao buscar usuário', color: 'warn' }));
	}

	@Action
	addOne(usuario: Usuario) {
		this.findedByID(usuario);
	}
}

export const UsuarioStore = new UsuarioVuex({ store, name: 'usuario' });

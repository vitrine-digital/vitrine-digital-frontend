import { VuexModule, Module, Mutation, Action } from 'vuex-class-modules';
import store from './index';
import GenericService from '@/services/generic.service';
import { Produto } from '@/models/produto';
import { EntityState, EntityAdapter } from 'vue-entity-adapter-plus';
import Pagination from '@/models/Pagination';
import Page from '@/models/Page';
import { openSnack } from '@/_helpers/utils';
import { ProdutoService } from '@/views/produto/produto.service';

interface State extends EntityState<Produto> {
	alterado: Produto | null;
	loadingList: boolean;
	totalItens: number;
}

const stateAdapter = new EntityAdapter<Produto>();
const service = new ProdutoService();

@Module
class ProdutoVuex extends VuexModule {
	// state
	state: State = {
		...stateAdapter.getInitialState(),
		alterado: null,
		loadingList: false,
		totalItens: 0,
	};

	// getters
	get produtos() {
		return this.state.entities;
	}

	get carregando() {
		return this.state.loadingList;
	}

	get alterado() {
		return this.state.alterado;
	}

	getOne(id: string | number) {
		return stateAdapter.getOne(id.toString(), this.state);
	}

	// mutations
	@Mutation
	private listado(payload: Page<Produto>) {
		let { entities, ids } = stateAdapter.addMany(payload.items, this.state);
		this.state.entities = entities;
		this.state.ids = ids;
	}

	@Mutation
	private salvo(payload: Produto) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Item cadastrado com sucesso!', color: 'success' });
	}

	@Mutation
	private atualizado(payload: Produto) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Item atualizado com sucesso!', color: 'success' });
	}

	@Mutation
	private setadoAlterado(payload: Produto) {
		this.state.alterado = payload;
	}

	@Mutation
	private findedByID(payload: Produto) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
	}

	@Mutation
	private imagensSalvas(payload: Produto) {
		this.state = <State>stateAdapter.addOne(payload, this.state);
		this.state.alterado = payload;
		openSnack({ msg: 'Imagem(s) salva(s) com sucesso!', color: 'success' });
	}

	// actions
	@Action
	async listar(params: any) {
		this.state.loadingList = true;
		service
			.list(params)
			.then((res) => {
				res.data.items.forEach((element) => {
					element.files = [];
				});
				this.listado(res.data);
				this.state.loadingList = false;
			})
			.catch((err) => {
				this.state.loadingList = false;
			});
	}

	@Action
	async salvar(produto: Produto) {
		service
			.save(produto)
			.then((res) => {
				res.data.files = [];
				this.salvo(res.data);
			})
			.catch((err) =>
				openSnack({ msg: 'Erro ao cadastrar item', color: 'warn' })
			);
	}

	@Action
	async atualizar(produto: Produto) {
		service
			.update(produto)
			.then((res) => {
				res.data.files = [];
				this.atualizado(res.data);
			})
			.catch((err) =>
				openSnack({ msg: 'Erro ao atualizar item', color: 'warn' })
			);
	}

	@Action
	setAlterado(produto: Produto) {
		this.setadoAlterado(produto);
	}

	@Action
	salvarImagens(data: { id: number; payload: any }): Promise<Produto> {
		return service
			.saveImagens(data.id, data.payload)
			.then((res) => {
				res.data.files = [];
				this.imagensSalvas(res.data);
				return res;
			})
			.catch((err) => {
				openSnack({ msg: 'Erro ao Salvar Imagem(s)', color: 'warn' });
				return null;
			});
	}

	@Action
	findById(id: number) {
		service
			.findById(id)
			.then((res) => {
				res.data.files = [];
				this.findedByID(res.data);
			})
			.catch((err) => openSnack({ msg: 'Erro ao buscar item', color: 'warn' }));
	}

	@Action
	addOne(produto: Produto) {
		this.findedByID(produto);
	}
}

export const ProdutoStore = new ProdutoVuex({ store, name: 'produto' });

export default class Pagination {
    page: number;
    limit: number;

    static build() {
        let obj = new Pagination();
        obj.limit = 10;
        obj.page = 1;
        return obj;
    }
}
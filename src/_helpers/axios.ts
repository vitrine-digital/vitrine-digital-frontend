import axios from 'axios';
import { AuthStore } from '@/store/auth.store';

const api = axios.create({
    baseURL: process.env.VUE_APP_URL_API  || 'http://localhost:3000'// TODO: Trocar para .env
});

// https://stackoverflow.com/questions/47946680/axios-interceptor-in-vue-2-js-using-vuex
api.interceptors.request.use(function(config) {
    const token = AuthStore.token;
    if(token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
}, function(err) {
    return Promise.reject(err);
});

export default api;
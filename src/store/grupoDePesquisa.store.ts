import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import store from "./index";
import GenericService from '@/services/generic.service';
import { Produto } from '@/models/produto';
import { EntityState, EntityAdapter } from 'vue-entity-adapter-plus';
import Pagination from '@/models/Pagination';
import Page from '@/models/Page';
import { GrupoDePesquisa } from '@/models/grupoDePesquisa';

interface State extends EntityState<GrupoDePesquisa> {
  loadingList: boolean;
  totalItens: number;
}

const stateAdapter = new EntityAdapter<GrupoDePesquisa>();
const service = new GenericService<GrupoDePesquisa>('grupos-de-pesquisa');

@Module
class GrupoDePesquisaVuex extends VuexModule {

  // state
  state: State = {
    ...stateAdapter.getInitialState(),
    loadingList: false,
    totalItens: 0
  };

  // getters
  get gruposDePesquisa() {
    return stateAdapter.getAll(this.state);
  }

  get carregando() {
    return this.state.loadingList;
  }

  // mutations
  @Mutation
  private listado(payload: Page<GrupoDePesquisa>) {
    let { entities, ids } = stateAdapter.addMany(payload.items, this.state);
    this.state.entities = entities;
    this.state.ids = ids;
  }

  /* @Mutation
  private changeToken(token: string) {
    this.state.token = token;
  } */

  // actions
  @Action
  async listar(pagination: Pagination) {
    this.state.loadingList = true;
    service
      .list(pagination)
      .then((res) => {
        this.listado(res.data);
        this.state.loadingList = false;
      })
      .catch((err) => {
        this.state.loadingList = false;
      });
  }

  /* @Action
  async setToken(token: string){
    this.changeToken(token);
  } */

  /* @Action
  async userChecked(payload: UsuarioState) {
    this.logged(payload);
  } */
}

// register module (could be in any file)

export const GrupoDePesquisaStore = new GrupoDePesquisaVuex({ store, name: "grupoDePesquisa" });

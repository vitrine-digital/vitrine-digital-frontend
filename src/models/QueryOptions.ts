import GenericConfig from './genericConfig';
import Pagination from './Pagination';

export default interface QueryOptions {
    config: GenericConfig;
    pagination: Pagination,
    query: object
}
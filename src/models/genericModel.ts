import EntidadeBaseSimples from './entidadeBaseSimples';

export interface GenericModel extends EntidadeBaseSimples {
    descricao: string
    sigla?: string;    
}
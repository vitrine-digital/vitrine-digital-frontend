import EntidadeBaseSimples from './entidadeBaseSimples';

export class FichaTecnica {
	tituloOriginal: string;
	origem: string;
	publicoAlvo: string;
	finalidade: string;
	areaDeConhecimento: EntidadeBaseSimples;
	organizacao: string;
	registro: string;
	avaliacao: string;
	disponibilidade: string;
	divulgacao: string;
	financiamento: string;
	url: string;
	idioma: string;
	cidade: string;
	pais: string;
	ano: string;
}

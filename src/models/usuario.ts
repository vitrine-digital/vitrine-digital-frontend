import EntidadeBaseSimples from './entidadeBaseSimples';
import { GrupoDePesquisa } from './grupoDePesquisa';

export class Usuario extends EntidadeBaseSimples {

	nome: string;
	sobrenome: string;
	email: string;
	senha: string;
	citacao: string;
	lattes: string;
	descricao: string;
	cpf: string;
	matricula: string;
	grupoDePesquisa: GrupoDePesquisa;
	orientador: Usuario;
	coorientador: Usuario;
	foto: string;
}
import { UsuarioLogin } from '@/models/usuarioLogin';
import { UsuarioCadastro } from '@/models/usuarioCadastro';
import { UsuarioState } from '@/store/auth.store';
import api from '../_helpers/axios';

export class AuthService {
    registerPath: string = 'register';
    loginPath: string = 'login';

    constructor(){
    }

    login(item: UsuarioLogin) {
        return api.post<UsuarioState>(`/auth/${this.loginPath}`, item)
    }

    registrar(item: UsuarioCadastro) {
        return api.post<UsuarioCadastro>(`/auth/${this.registerPath}`, item);
    }
    
    checkToken() {
        return api.post<UsuarioState>(`/auth/check`);
    }
}
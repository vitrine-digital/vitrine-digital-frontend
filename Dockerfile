FROM nginx:1.16-alpine
COPY default.conf /etc/nginx/conf.d
COPY ./dist /usr/share/nginx/html

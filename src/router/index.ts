import { recursosGenericos } from "@/views/crud-generico/base.config";
import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import { authGuard } from "../_helpers/navigationGuards";
import { produtoResolver } from '@/views/produto/resolver';
import multiguard from 'vue-router-multiguard';
import Home from '../views/home-page/Home.vue'

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/linhas-de-pesquisa",
    name: "linhas",
    props: { config: recursosGenericos["linhas-de-pesquisa"] },
    beforeEnter: authGuard,
    component: () => import("../views/crud-generico/listar/Listar.vue")
  },
  {
    path: "/areas-de-conhecimento",
    name: "areas",
    props: { config: recursosGenericos["areas-de-conhecimento"] },
    component: () => import("../views/crud-generico/listar/Listar.vue"),
  },
  {
    path: "/login",
    name: "login",
    beforeEnter: authGuard,
    component: () => import("../views/login/Login.vue"),
  },
  {
    path: "/produtos",
    name: "produtos",
    component: () => import("../views/produto/Listar.vue"),
  },
  /* TODO: Observar o padrão abaixo (atualizar e não mudar componente) */
  {
    path: "/produtos/cadastrar",
    name: "produtos/cadastro",
    component: () => import("../views/produto/CadastrarEditar.vue"),
  },
  {
    path: "/produtos/:id/editar",
    name: "produtos/edicao",
    component: () => import("../views/produto/CadastrarEditar.vue"),
    beforeEnter: multiguard([authGuard, produtoResolver])
  },
  {
    path: "/meu-perfil",
    name: "meu-perfil",
    component: () => import("../views/usuario/perfil/Perfil.vue"),
    beforeEnter: multiguard([authGuard])
  },
  {
    path: "/visualizar/:id",
    name: "visualizar",
    component: () => import("../views/home-page/produto-detalhe/ProdutoDetalhe.vue"),
    beforeEnter: produtoResolver
  },
  {
    path: "/usuarios",
    name: "usuarios",
    beforeEnter: authGuard,
    component: () => import("../views/usuario/listar/Listar.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

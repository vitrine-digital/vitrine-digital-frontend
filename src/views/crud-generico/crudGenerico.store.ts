import EntidadeBaseSimples from "@/models/entidadeBaseSimples";
import { GenericModel } from "@/models/genericModel";
import Pagination from "@/models/Pagination";
import QueryOptions from "@/models/QueryOptions";
import GenericService from "@/services/generic.service";
import { EntityAdapter, EntityState } from "vue-entity-adapter-plus";
import { ActionTree, GetterTree, MutationTree } from "vuex";
import Page from "@/models/Page";
import { openSnack } from '@/_helpers/utils';

interface State extends EntityState<EntidadeBaseSimples> {
  itens: EntidadeBaseSimples[];
  loadingList: boolean;
  totalItens: number;
}

const sortName = (a: GenericModel, b: GenericModel) => a.nome.localeCompare(b.nome)
const stateAdapter = new EntityAdapter<EntidadeBaseSimples>();
const service = new GenericService<EntidadeBaseSimples>("");

const genericState: State = {
  ...stateAdapter.getInitialState(),
  itens: [],
  loadingList: false,
  totalItens: 0,
};

const genericGetters: GetterTree<State, any> = {
  allItens: (state) => stateAdapter.getAll(state),
  itens: (state) => state.itens,
  totalItens: (state) => state.totalItens,
  loadingList: (state) => state.loadingList,
  getOne(state) {
    return (id: number) => stateAdapter.getOne(id.toString(), state); 
  }
};

const mutations: MutationTree<State> = {
  listado(state, data: MutationData<EntidadeBaseSimples>) {
    let { entities, ids } = stateAdapter.addMany(data.page.items, state);
    state.entities = entities;
    state.ids = ids;
    state.totalItens = data.page.totalItems;
    let pagination = data.options.pagination;
    if (pagination.limit == 0 || pagination.page == 0) {
      state.itens = stateAdapter.getAll(state);
    } else {
      state.itens = stateAdapter
        .getAll(state)
        .slice(
          pagination.page * pagination.limit - pagination.limit,
          pagination.page * pagination.limit
        );
    }
  },

  previusPage(state, pagination: Pagination) {
    if (pagination.limit == 0 || pagination.page == 0) {
      state.itens = stateAdapter.getAll(state);
    } else {
      state.itens = stateAdapter
        .getAll(state)
        .slice(
          pagination.page * pagination.limit - pagination.limit,
          pagination.page * pagination.limit
        );
    }
  },

  salvo(state, payload: MutationAddOne) {
    let { entities, ids } = stateAdapter.addOne(payload.item, state);
    state.entities = entities;
    state.ids = ids;
    state.totalItens = state.totalItens + 1;
    let pagination = payload.pagination;
    state.itens = stateAdapter
      .getAll(state)
      .slice(
        pagination.page * pagination.limit - pagination.limit,
        pagination.page * pagination.limit
      );
      openSnack({msg: 'Cadastro realizado com sucesso!', color: 'success'})
  },
};

const actions: ActionTree<State, any> = {
  listarGenerico({ commit, state, getters }, options: QueryOptions) {
    service.endpoint = options.config.endpoint;
    if (options.pagination.limit == -1) {
      options.pagination.limit = 0;
    }
    state.loadingList = true;
    service
      .list({...options.pagination, ...options.query})
      .then((res) => {
        commit("listado", { page: res.data, options });
        state.loadingList = false;
      })
      .catch((err) => {
        state.loadingList = false;
      });
    
  },

  salvar({ commit, state }, data: MutationAddOne) {
    service
      .save(data.item)
      .then((res) => {
        commit("salvo", { item: res.data, pagination: data.pagination });
        state.loadingList = false;
      })
      .catch((err) => {
        state.loadingList = false;
      });
  },

  atualizar({ commit, state, getters }, options: QueryOptions) {
    service.endpoint = options.config.endpoint;
    state.loadingList = true;
    service
      .list({ page: 0, limit: stateAdapter.getCount(state) })
      .then((res) => {
        commit("listado", { page: res.data, options });
        state.loadingList = false;
      })
      .catch((err) => {
        state.loadingList = false;
      });
  },
};

function canRequest(state: typeof genericState, pagination: Pagination) {
  const countAllItens = stateAdapter.getCount(state);
  const totalItens = state.totalItens;
  if (!countAllItens) {
    return true;
  }
  if (pagination.limit == 0 && countAllItens < totalItens) {
    return true;
  }
  if (
    countAllItens - pagination.page * pagination.limit <= 0 &&
    countAllItens < totalItens
  ) {
    return true;
  }
  return false;
}

export function newInstance() {
  return {
    namespaced: true,
    // Estou utilizando um Store Generico, preciso devolver o estado por meio de função para que o estado não seja compartilhado
    state: () => {
      return { ...genericState };
    },
    mutations,
    actions,
    getters: genericGetters,
  };
}

export interface MutationData<T> {
  page: Page<T>;
  options: QueryOptions;
}

export interface MutationAddOne {
  item: GenericModel;
  pagination: Pagination;
}

import GenericConfig from '@/models/genericConfig';

export const recursosGenericos: { [key: string]: GenericConfig }  = {
    /* Adicione um novo objeto com as mesmas informações dos já existentes aqui para
        utilizar os poderes do CRUD genérico */
    'linhas-de-pesquisa': {
        endpoint: 'linhas-de-pesquisa',
        listar: 'Linhas de Pesquisa',
        single: 'Linha de Pesquisa',
        store: 'linhaDePesquisa',
        getter: 'itens'
    } as GenericConfig,
    'areas-de-conhecimento': {
        endpoint: 'areas-de-conhecimento',
        listar: 'Áreas de Conhecimento',
        single: 'Área de Conhecimento',
        store: 'areaDeConhecimento',
        getter: 'itens'
    } as GenericConfig,
    'usuarios': {
        endpoint: 'usuarios',
        listar: 'Usuários',
        single: 'Usuário',
        store: 'usuario',
        getter: 'usuarios'
    } as GenericConfig
}
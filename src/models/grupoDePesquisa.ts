import EntidadeBaseSimples from './entidadeBaseSimples';
import { GenericModel } from './genericModel';

export interface GrupoDePesquisa extends EntidadeBaseSimples {
    linhaDePesquisa: GenericModel
}
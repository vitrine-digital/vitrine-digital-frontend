import api from '../_helpers/axios';
import Page from '@/models/Page';
import Pagination from '@/models/Pagination';

interface EntidadeBase {
    id: number
}

export default class GenericService<T extends EntidadeBase> {
    endpoint: string = ''

    constructor(recurso: string){
        this.endpoint = recurso;
    }

    save(item: T) {
        
        return api.post<T>(`${this.endpoint}`, item);
    }

    update(item: T) {
        return api.put<T>(`${this.endpoint}/${item.id}`, item);
    }

    list(params: any) {
        if(params.limit < 0) params.limit = 0;
        return api.get<Page<T>>(`/${this.endpoint}`, {params});
    }

    delete(id: number) {
        return api.delete(`${this.endpoint}/${id}`);
    }

    findById(id: number) {
        return api.get<T>(`${this.endpoint}/${id}`);
    }
}
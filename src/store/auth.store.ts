import { VuexModule, Module, Mutation, Action } from "vuex-class-modules";
import { Usuario } from "@/models/usuario";
import { UsuarioLogin } from "@/models/usuarioLogin";
import { AuthService } from "@/services/auth.service";
import store from "./index";
import { openSnack } from '@/_helpers/utils';
import { UsuarioService } from '@/views/usuario/perfil/usuario.service';

export interface UsuarioState {
  usuario: Usuario;
  loging: boolean | false;
  token: string | null;
}

// const usuarioAdapter = new EntityAdapter<Usuario>();

@Module
class AuthVuex extends VuexModule {
  private readonly service = new AuthService();
  private readonly usuarioService = new UsuarioService();

  // state
  state: UsuarioState = {
    usuario: null,
    token: null,
    loging: false,
  };

  // getters
  get usuario() {
    return this.state.usuario;
  }

  get token() {
    return this.state.token;
  }

  // mutations
  @Mutation
  private logged(payload: UsuarioState) {
    this.state.usuario = payload.usuario;
    this.state.token = payload.token;
    localStorage.setItem('accessToken', <string>payload.token);
  }

  @Mutation
  private changeToken(token: string) {
    this.state.token = token;
  }

  @Mutation
  private changeUsuario(usuario: Usuario) {
    this.state.usuario = usuario;
  }

  // actions
  @Action
  async initLogin(dadosLogin: UsuarioLogin) {
    this.service
      .login(dadosLogin)
      .then((res) => {
        this.logged(res.data);
        this.state.loging = false;
        openSnack({msg: 'Login realizado com sucesso', color: 'success'})
      })
      .catch((err) => {
        this.state.loging = false;
        openSnack({msg: 'Credenciais incorretas', color: 'error'})
      });
  }

  @Action
  async setToken(token: string){
    this.changeToken(token);
  }

  @Action
  async userChecked(payload: UsuarioState) {
    this.logged(payload);
  }

  @Action
  async atualizarUsuario(data: {id: number, form: FormData}) {
    this.usuarioService.updateUsuario(data.id, data.form)
      .then(
        res => {
          this.changeUsuario(res.data)
          openSnack({msg: 'Alterações realizadas com sucesso', color: 'success'})
        }
      ).catch(
        err => {}
      )
  }
}

// register module (could be in any file)

export const AuthStore = new AuthVuex({ store, name: "auth" });

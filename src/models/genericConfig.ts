export default interface GenericConfig {
    endpoint: string;
    listar: string;
    single: string;
    store: string;
    getter: string;
}
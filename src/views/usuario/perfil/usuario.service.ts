import GenericService from '@/services/generic.service';
import api from '@/_helpers/axios';
import { Usuario } from '@/models/usuario';

export class UsuarioService extends GenericService<Usuario> {
    constructor() {
        super('usuarios');
    }

    updateUsuario(id: number, form: FormData) {
        // TODO: Tentar tornar uma constant
        const header = {
            headers: {
                'Content-type': 'multipart/form-data',
            },
        }
        return api.put<Usuario>(`${this.endpoint}/${id}`, form, header);
    }

    
}
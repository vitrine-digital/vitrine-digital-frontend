import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import Vue2Filters from 'vue2-filters';
import VueYoutube from 'vue-youtube';

Vue.use(Vue2Filters);

Vue.use(VueYoutube);

Vue.config.productionTip = false;

new Vue({
	router,
	store,
	vuetify,
	render: (h) => h(App),
}).$mount('#app');
